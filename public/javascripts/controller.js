var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $http) {


    $scope.search = function () {
        $scope.info = "";

        var marvelAPI = 'https://gateway.marvel.com/v1/public/characters?nameStartsWith=' + $scope.test + '&apikey=a12ba9c7783cdf31f0f508bd55b07f46';

        $http({
            method: 'GET',
            url: marvelAPI
        }).then(function successCallback(response) {
            $scope.testing = response.data;

        }, function errorCallback(response) {

        });
    }

    $scope.details = function (id) {
        $scope.name = $scope.testing.data.results[id].name;
        $scope.desc = $scope.testing.data.results[id].description;
        $scope.url = $scope.testing.data.results[id].urls;
        $scope.testing = ""
    }



});
